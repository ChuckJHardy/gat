# frozen_string_literal: true

class OutputView
  def initialize(title:, collection:)
    @title = title
    @collection = collection
  end

  def to_s
    [title, body].join("\n")
  end

  private

  def title
    "#{@title}:"
  end

  def body
    @collection.map(&:to_s).join("\n")
  end
end
