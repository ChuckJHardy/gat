# frozen_string_literal: true

OutputCollectionView = Struct.new(:views) do
  def to_s
    views.map(&:to_s).join("\n\n")
  end
end
