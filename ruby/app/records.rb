# frozen_string_literal: true

Records = Struct.new(:records) do
  def sort_by_campus_and_last_name
    records.sort_by { |record| [record.campus, record.last_name] }
  end

  def sort_by_date_of_birth
    records.sort_by(&:date_of_birth)
  end

  def sort_by_last_name
    records.sort_by(&:last_name)
  end
end
