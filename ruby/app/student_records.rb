# frozen_string_literal: true

require 'date'

require_relative 'records'
require_relative 'models/comma'
require_relative 'models/dollar'
require_relative 'models/pipe'
require_relative 'services/importer'
require_relative 'decorators/record_decorator'
require_relative 'views/output_view'
require_relative 'views/output_collection_view'

class StudentRecords
  def initialize(comma_file_path:, dollar_file_path:, pipe_file_path:)
    @comma_file_path = comma_file_path
    @dollar_file_path = dollar_file_path
    @pipe_file_path = pipe_file_path
  end

  def print
    puts to_s
  end

  def to_s
    OutputCollectionView.new(views).to_s
  end

  def records
    @records ||= Records.new(importer_records)
  end

  def importer_records
    (comma + dollar + pipe).map(&RecordDecorator)
  end

  def comma
    Importer.call(@comma_file_path, Comma)
  end

  def dollar
    Importer.call(@dollar_file_path, Dollar)
  end

  def pipe
    Importer.call(@pipe_file_path, Pipe)
  end

  private

  def views
    [
      OutputView.new(title: 'Output 1', collection: records.sort_by_campus_and_last_name),
      OutputView.new(title: 'Output 2', collection: records.sort_by_date_of_birth),
      OutputView.new(title: 'Output 3', collection: records.sort_by_last_name.reverse)
    ]
  end
end
