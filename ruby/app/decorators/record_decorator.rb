# frozen_string_literal: true

require 'delegate'

class RecordDecorator < SimpleDelegator
  def self.to_proc
    ->(record) { new(record) }
  end

  def to_s
    [
      last_name,
      first_name,
      campus,
      formatted_date_of_birth,
      favorite_color
    ].join(' ')
  end

  def campus
    {
      'LA' => 'Los Angeles',
      'NYC' => 'New York City',
      'SF' => 'San Francisco'
    }.fetch(super.upcase, super)
  end

  def formatted_date_of_birth
    date_of_birth.strftime('%-m/%-d/%Y')
  end
end
