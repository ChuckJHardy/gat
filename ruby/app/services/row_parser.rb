# frozen_string_literal: true

RowParser = Struct.new(:row) do
  IMPORTER_DELIMITERS = /,|\s[\$\|]\s/

  def self.to_proc
    ->(row) { new(row).parse }
  end

  def parse
    row.split(IMPORTER_DELIMITERS).map(&:strip)
  end
end
