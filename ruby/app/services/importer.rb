# frozen_string_literal: true

require_relative 'row_parser'

Importer = Struct.new(:file_path, :model) do
  def self.call(*args)
    new(*args).call
  end

  def call
    file.map(&RowParser).map(&model)
  end

  private

  def file
    File.readlines(file_path)
  end
end
