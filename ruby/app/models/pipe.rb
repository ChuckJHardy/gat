# frozen_string_literal: true

require_relative 'concerns/common_model'

Pipe = Struct.new(
  :last_name,
  :first_name,
  :middle_initial,
  :campus,
  :favorite_color,
  :date_of_birth
) { include CommonModel }
