# frozen_string_literal: true

module CommonModel
  def self.included(klass)
    klass.extend(ClassMethods)
  end

  def initialize(*)
    super
    self.date_of_birth = parsed_date
  end

  def parsed_date
    Date.strptime(date_of_birth, imported_date_format)
  end

  def imported_date_format
    '%m-%d-%Y'
  end

  module ClassMethods
    def to_proc
      ->(args) { new(*args) }
    end
  end
end
