# frozen_string_literal: true

require_relative 'concerns/common_model'

Comma = Struct.new(
  :last_name,
  :first_name,
  :campus,
  :favorite_color,
  :date_of_birth
) do
  include CommonModel

  def imported_date_format
    '%m/%d/%Y'
  end
end
