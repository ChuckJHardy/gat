# frozen_string_literal: true

require_relative 'concerns/common_model'

Dollar = Struct.new(
  :last_name,
  :first_name,
  :middle_initial,
  :campus,
  :date_of_birth,
  :favorite_color
) { include CommonModel }
