# frozen_string_literal: true

RSpec.describe Importer do
  subject(:importer) { described_class.call(file_path, model) }

  let(:file_path) { './spec/fixtures/comma.txt' }
  let(:model) { Comma }

  it 'returns collection of objects' do
    expect(importer.map(&:class)).to eq([model, model, model])
  end
end
