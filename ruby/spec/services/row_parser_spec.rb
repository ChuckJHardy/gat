# frozen_string_literal: true

RSpec.describe RowParser do
  subject(:parser) { [row].map(&described_class).first }

  let(:collection) { %w(Kirlin Mckayla Atlanta Maroon 5/29/1986) }

  context 'when element seperator is a comma' do
    let(:row) { collection.join(',') }

    it 'returns expected collection of seperate elements' do
      expect(parser).to eq(collection)
    end
  end

  context 'when element seperator is a dollar with a prepended whitespace' do
    let(:row) { collection.join(' $ ') }

    it 'returns expected collection of seperate elements' do
      expect(parser).to eq(collection)
    end
  end

  context 'when element seperator is a pipe with a prepended whitespace' do
    let(:row) { collection.join(' | ') }

    it 'returns expected collection of seperate elements' do
      expect(parser).to eq(collection)
    end
  end

  context 'when format includes extra spaces and newlines' do
    let(:row) { "  Kirlin,   Mckayla,   Atlanta,  Maroon, 5/29/1986\n" }

    it 'returns expected collection of seperate elements' do
      expect(parser).to eq(collection)
    end
  end
end
