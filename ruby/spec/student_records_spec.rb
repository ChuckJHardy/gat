# frozen_string_literal: true

RSpec.describe StudentRecords do
  subject(:student_records) do
    described_class.new(
      comma_file_path: './spec/fixtures/comma.txt',
      dollar_file_path: './spec/fixtures/dollar.txt',
      pipe_file_path: './spec/fixtures/pipe.txt'
    )
  end

  describe '#print' do
    subject(:print) { student_records.print }

    it 'prints output to screen' do
      expect(student_records).to receive(:puts).with(student_records.to_s)
      print
    end
  end

  describe '#to_s' do
    subject(:to_s) { student_records.to_s }

    let(:expected_outlet) do
      File.read('./spec/fixtures/student_records.txt').chomp
    end

    it 'returns student records in a formatted output' do
      expect(to_s).to eq(expected_outlet)
    end
  end

  describe '#records' do
    subject(:records) { student_records.records }

    it 'returns records object' do
      expect(records).to be_an_instance_of(Records)
    end
  end

  describe '#comma' do
    subject(:comma) { student_records.comma }

    it 'returns a collection of comma objects' do
      expect(comma.map(&:class)).to eq([Comma, Comma, Comma])
    end
  end

  describe '#dollar' do
    subject(:dollar) { student_records.dollar }

    it 'returns a collection of dollar objects' do
      expect(dollar.map(&:class)).to eq([Dollar, Dollar, Dollar])
    end
  end

  describe '#pipe' do
    subject(:pipe) { student_records.pipe }

    it 'returns a collection of pipe objects' do
      expect(pipe.map(&:class)).to eq([Pipe, Pipe, Pipe])
    end
  end
end
