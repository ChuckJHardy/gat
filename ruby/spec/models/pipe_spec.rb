# frozen_string_literal: true

RSpec.describe Pipe do
  subject(:adapter) { [row].map(&described_class).first }

  let(:row) { %w(Goyette Timmothy E London Pacific\ Blue 10-2-1964) }

  it 'returns expected response' do
    expect(adapter).to have_attributes(
      last_name: 'Goyette',
      first_name: 'Timmothy',
      middle_initial: 'E',
      campus: 'London',
      favorite_color: 'Pacific Blue',
      date_of_birth: Date.parse('1964-10-02')
    )
  end
end
