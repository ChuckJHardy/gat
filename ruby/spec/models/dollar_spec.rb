# frozen_string_literal: true

RSpec.describe Dollar do
  subject(:adapter) { [row].map(&described_class).first }

  let(:row) { %w(Wilkinson Stacy D NYC 1-22-1964 Shocking\ Pink) }

  it 'returns expected response' do
    expect(adapter).to have_attributes(
      last_name: 'Wilkinson',
      first_name: 'Stacy',
      middle_initial: 'D',
      campus: 'NYC',
      date_of_birth: Date.parse('1964-01-22'),
      favorite_color: 'Shocking Pink'
    )
  end
end
