# frozen_string_literal: true

RSpec.describe Comma do
  subject(:adapter) { [row].map(&described_class).first }

  let(:row) { %w(Kirlin Mckayla Atlanta Maroon 5/29/1986) }

  it 'returns expected response' do
    expect(adapter).to have_attributes(
      last_name: 'Kirlin',
      first_name: 'Mckayla',
      campus: 'Atlanta',
      favorite_color: 'Maroon',
      date_of_birth: Date.parse('1986-05-29')
    )
  end
end
