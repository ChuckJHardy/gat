# frozen_string_literal: true

RSpec.describe OutputCollectionView do
  subject(:collection_view) { described_class.new(views).to_s }

  let(:views) { [view, view] }
  let(:view) { OutputView.new(title: 'Outlet 1', collection: []) }

  let(:expected_output) do
    "Outlet 1:\n\n\nOutlet 1:\n"
  end

  it 'returns expected output' do
    expect(collection_view).to eq(expected_output)
  end
end
