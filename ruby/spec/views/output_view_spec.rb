# frozen_string_literal: true

RSpec.describe OutputView do
  subject(:view) do
    described_class.new(title: title, collection: collection).to_s
  end

  let(:title) { 'Output 1' }
  let(:collection) { [comma, comma] }
  let(:comma) do
    RecordDecorator.new(
      Comma.new(*%w(Kirlin Mckayla Atlanta Maroon 5/29/1986))
    )
  end

  let(:expected_output) do
    "Output 1:\nKirlin Mckayla Atlanta 5/29/1986 Maroon\nKirlin Mckayla Atlanta 5/29/1986 Maroon"
  end

  it 'returns expected output' do
    expect(view).to eq(expected_output)
  end
end
