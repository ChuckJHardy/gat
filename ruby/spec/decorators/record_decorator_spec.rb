# frozen_string_literal: true

RSpec.describe RecordDecorator do
  subject(:decorator) { [comma].map(&described_class).first }

  let(:comma_campus) { 'la' }
  let(:comma) do
    Comma.new(*%W(Kirlin Mckayla #{comma_campus} Maroon 5/29/1986))
  end

  describe '#to_s' do
    subject(:to_s) { decorator.to_s }

    it 'returns object represented as a formatted string' do
      expect(to_s).to eq('Kirlin Mckayla Los Angeles 5/29/1986 Maroon')
    end
  end

  describe '#campus' do
    subject(:campus) { decorator.campus }

    it 'adapts LA' do
      expect(campus).to eq('Los Angeles')
    end

    context 'when campus is not adaptable' do
      let(:comma_campus) { 'will_not_exist' }

      it 'returns campus' do
        expect(campus).to eq(comma_campus)
      end
    end
  end

  describe '#formatted_date_of_birth' do
    subject(:formatted_date_of_birth) { decorator.formatted_date_of_birth }

    it 'returns string representation of #date_of_birth' do
      expect(formatted_date_of_birth).to eq('5/29/1986')
    end
  end
end
