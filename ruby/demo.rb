# frozen_string_literal: true

require './app/student_records'

StudentRecords.new(
  comma_file_path: './spec/fixtures/comma.txt',
  dollar_file_path: './spec/fixtures/dollar.txt',
  pipe_file_path: './spec/fixtures/pipe.txt'
).print
